package local.sandbox.spoj.pl.potsam;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * POTSAM - Samolot
 * <p>
 * Bajtockie Linie Lotnicze wzbogaciły swoją flotę o nowy model samolotu. W samolocie tym jest n1 rzędów miejsc
 * siedzących w klasie biznesowej oraz n2 rzędów w klasie ekonomicznej. W klasie biznesowej każdy rząd ma k1 miejsc
 * siedzących, a w klasie ekonomicznej — k2 miejsc.
 * <p>
 * Zadanie
 * Napisz program, który:
 * wczyta informacje na temat dostępnych miejsc siedzących w samolocie,
 * wyznaczy sumaryczną liczbę wszystkich miejsc siedzących,
 * wypisze wynik
 * <p>
 * Wejście
 * W pierwszym i jedynym wierszu wejścia znajdują się cztery liczby naturalne n1, k1, n2, i k2 (1<=n1,k1,n2,k2<=1000),
 * pooddzielane pojedynczymi odstępami.
 * <p>
 * Wyjście
 * Pierwszy i jedyny wiersz wyjścia powinien zawierać jedną liczbę całkowitą - liczbę miejsc siedzących w analizowanym
 * samolocie.
 * Przykład
 * Wejście
 * 2 5 3 20
 * Wyjście
 * 70
 */
public class Potsam {
	public static void main(String... args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String[] params = reader.readLine().split(" ");
		reader.close();

		int n1 = Integer.parseInt(params[0]);
		int k1 = Integer.parseInt(params[1]);
		int n2 = Integer.parseInt(params[2]);
		int k2 = Integer.parseInt(params[3]);

		if ((n1 < 1 || n1 > 1000) ||
				(k1 < 1 || k1 > 1000) ||
				(n2 < 1 || n2 > 1000) ||
				(k2 < 1 || k2 > 1000)
		) {
			throw new Exception("Wrong input data");
		}

		System.out.print(n1 * k1 + n2 * k2);
	}
}
