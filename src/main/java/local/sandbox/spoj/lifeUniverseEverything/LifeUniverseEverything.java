package local.sandbox.spoj.lifeUniverseEverything;

import java.util.Scanner;

/**
 * LifeUniverseEverything
 */
public class LifeUniverseEverything {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		String s;

		do {
			s = in.next();
			System.out.println(s);
		} while (!s.equals("42"));
	}
}
