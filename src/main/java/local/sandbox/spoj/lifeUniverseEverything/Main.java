package local.sandbox.spoj.lifeUniverseEverything;

/**
 * Life, the Universe, and Everything (1)
 *
 * Your program is to use the brute-force approach in order to find the Answer
 * to Life, the Universe, and Everything. More precisely... rewrite small
 * numbers from input to output. Stop processing input after reading in
 * the number 42. All numbers at input are integers of one or two digits.
 */
public class Main {

	/**
	 * @param args
	 * @throws java.lang.Exception
	 */
	public static void main(String[] args) throws java.lang.Exception {

		java.io.BufferedReader r = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		String s;

		while (!(s = r.readLine()).startsWith("42")) {
			System.out.println(s);
		}
	}
}
