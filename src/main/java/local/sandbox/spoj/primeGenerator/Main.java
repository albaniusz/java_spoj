package local.sandbox.spoj.primeGenerator;

import java.io.IOException;

/**
 * Prime Generator (2)
 *
 * Peter wants to generate some prime numbers for his cryptosystem. Help him!
 * Your task is to generate all prime numbers between two given numbers!
 *
 * Input
 * The input begins with the number t of test cases in a single line (t<=10).
 * In each of the next t lines there are two numbers m and n
 * (1 <= m <= n <= 1000000000, n-m<=100000) separated by a space.
 *
 * Output
 * For every test case print all prime numbers p such that m <= p <= n, one
 * number per line, test cases separated by an empty line.
 *
 * Example
 * Input:
 * 2
 * 1 10
 * 3 5
 *
 * Output:
 * 2
 * 3
 * 5
 * 7
 *
 * 3
 * 5
 */
public class Main {

	/**
	 * @param n
	 * @return
	 */
	public static boolean isPrime(int n) {

		//check if n is a multiple of 2
		if (n % 2 == 0) return false;
		//if not, then just check the odds
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		java.io.BufferedReader r = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		String s;

		java.util.LinkedList<String> numbers = new java.util.LinkedList<String>();

		while (!(s = r.readLine()).startsWith("0")) {
			java.util.regex.Pattern p = java.util.regex.Pattern.compile("\\d+");
			java.util.regex.Matcher m = p.matcher(s);
			while (m.find()) {
				numbers.add(m.group());
			}

			System.out.println(numbers);
		}
	}
}
